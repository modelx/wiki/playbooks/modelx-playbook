#!/bin/bash
echo "Updating component modelx-common-doc..."
echo "======================================="

cd /c/gitlab/modelx/wiki/components/common-doc/modelx-common-doc
git checkout master
git add .
git commit -m "Dev"
git push origin master
echo ""
echo ""
echo "Updating playbook..."
echo "===================="
cd /c/gitlab/modelx/wiki/playbooks/modelx-playbook
git checkout master
git add .
git commit -m "Dev"
git push origin master
echo ""
echo ""
echo "Running Antora..."
echo "================="
antora --fetch antora-playbook.yml --attribute lang=no 	--attribute wysiwig_editing=0 
echo ""
echo ""
echo "Updating output..."
echo "=================="
#cd /c/gitlab/modelx/wiki/output/gitlab-pages/modelx.gitlab.io 
cd /c/gitlab/modelx/wiki/output/github-pages/modelx-no.github.io 

#Make it work with github:
touch .nojekyll
#
git checkout main
git add .
git commit -m "Dev"
git push origin main

